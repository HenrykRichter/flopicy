# FlopICY

I2C (TWI) controllers for the Commodore Amiga computers have gotten some attention in the a1k forum lately.
This particular project is a variant that plugs into the DB23 Floppy port externally. The original schematics
can be found in the package docs/hard/i2clib40.lha on Aminet. This recreation comes along with a readily available
PCB layout, Gerber files and a matching case for 3D printing.

The software interface is provided by the i2c.library.disk to be found in the aforementioned i2clib40.lha package.
The library needs to be renamed to i2c.library and goes into LIBS:

## Pictures

![PCB](https://gitlab.com/HenrykRichter/flopicy/raw/master/Pics/PCB_front.jpg)

![Attached to A500](https://gitlab.com/HenrykRichter/flopicy/raw/master/Pics/flopicy_a500.jpg)

![3D printed case](https://gitlab.com/HenrykRichter/flopicy/raw/master/Pics/flopicy_incase.jpg)

## Parts list
- 1 74xx01D (SOIC-14, e.g. 74LS01D,74HCT01D,74HC01D)
- 2 Resistor 4.7k (0805)
- 2 Resistor 10k (0805)
- 1 Capacitor 100nF (0805)
- 2 Pin strip RM2.54 (0.1") 4 position (angled)
- 1 DB23M solder cup connector

## Case screws
- 2 M2,5 x 10mm cylinder head screws
- 2 M2,5 nuts
